<?php

namespace Database\Seeders;

use App\Models\Url;
use App\Models\User;
use Faker\Factory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UrlSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $users = User::all();
        for($i=0;$i<500;$i++){
            $hits = random_int(1, 10);
            $date = date("Y/m/d", rand(strtotime('2022/01/01'), strtotime('2023/01/01')));
            $url = Url::create([
                "user_id" => $users->random()->id,
                "title" => Factory::create()->word(),
                "actual_url" => "www.google.com",
                "slug" => Url::generateSlug(),
                "hits" => $hits,
                'created_at' => $date,
                'updated_at' => $date
            ]);
            while ($hits--){
                $url->urlDetails()->create([
                    "ip" => '192.168.255.123',
                    "location" => Factory::create()->word(),
                ]);
            }
        }
    }
}

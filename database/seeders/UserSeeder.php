<?php

namespace Database\Seeders;

use App\Models\User;
use Faker\Factory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
//        $phoneNumbers = ->unique()->phoneNumber();
        for($i=0;$i<10;$i++){
            User::create([
                "name" => $faker->name(),
                "phone" => $faker->unique()->phoneNumber(),
                "email" => $faker->unique()->email(),
                "username" => $faker->unique()->word(),
                "password" => password_hash("password", PASSWORD_DEFAULT),
            ]);
        }
    }
}

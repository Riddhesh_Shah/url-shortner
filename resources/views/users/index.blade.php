@extends('layouts.app')

@section('page-title')
Index
@endsection

@section('content')
    <div class="mt-5">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">Most Visited URLs</div>
                    <div class="card-body">
                        <canvas id="most_visited_urls_chart"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">Total URL Views By Month</div>
                    <div class="card-body">
                        <canvas id="views_by_month_chart"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-5">
        <div class="card">
            <div class="card-header">URL Collection</div>
            <div class="card-body">
                <table id="table_id" class="display">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Actual URL</th>
                            <th>Short URL</th>
                            <th>Click Count</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($urls as $url)
                            <tr>
                                <td>{{$url->title}}</td>
                                <td><a href="{{$url->actual_url}}">{{$url->actual_url}}</a></td>
                                <td><a href="www.shorturl.com/{{$url->slug}}">www.shorturl.com/{{$url->slug}}</a></td>
                                <td>{{$url->clickCount()}}</td>
                                <td>
                                    <div class="btn btn-info btn-edit" id="btn_edit" data-id="{{$url->id}}" data-toggle="modal" data-target="#editUrlModal" onclick="fillEditUrlDetails(this)">
                                        <span class="fa fa-pen"></span>
                                    </div>
                                    <div class="btn btn-danger btn-delete " data-id="{{$url->id}}" data-toggle="modal" data-target="#deleteUrlModal" onclick="fillDeleteUrlDetails(this)">
                                        <span class="fa fa-trash"></span>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    {{--Create URL Modal--}}
    <div class="modal fade" id="createUrlModal" tabindex="-1" aria-labelledby="createUrlModalLabel" aria-hidden="true">
        <form action="{{route('user.urls.store', \Illuminate\Support\Facades\Auth::user())}}" method="POST" id="create_url_form">
            @csrf
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="createUrlModalLabel">Create A URL</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group mt-3">
                            <label for="title" class="form-label">Title for URL <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="title" name="title" placeholder="Enter title">
                            @error('title')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group mt-3">
                            <label for="actual-url" class="form-label">Actual URL <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="actual-url" name="actual_url" placeholder="https:://www.yourwebsite.com">
                            @error('actual_url')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-check mt-3">
                            <input class="form-check-input" type="checkbox" value="checked" name="custom_url_check" id="custom-url-check" onchange="customUrlField(this)">
                            <label class="form-check-label" for="flexCheckChecked">
                                Do you want to create a custom URL?
                            </label>
                        </div>
                        <div class="form-group" id="custom-url-card">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" id="create_url_btn" name="create_url_btn">Create URL</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    {{--Create URL Modal--}}

    {{--Edit URL Modal--}}
    <div class="modal fade" id="editUrlModal" tabindex="-1" aria-labelledby="editUrlModalLabel" aria-hidden="true">
        <form action="" method="POST" id="edit_url_form">
            @csrf
            @method('PUT')
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="editUrlModalLabel">Edit A URL</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group mt-3">
                            <label for="edit_url_title" class="form-label">Title for URL <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="edit_url_title" name="edit_url_title" placeholder="Enter title">
                            <input type="hidden" name="edit_url_id" id="edit_url_id">
                        </div>
{{--                        <div class="form-group mt-3">--}}
{{--                            <label for="edit_actual_url" class="form-label">Actual URL <span class="text-danger">*</span></label>--}}
{{--                            <input type="text" class="form-control" id="edit_actual_url" name="edit_actual_url" placeholder="Enter actual url">--}}
{{--                        </div>--}}
                        <div class="form-group mt-3">
                            <label for="edit_custom_url" class="form-label">Custom URL <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="edit_custom_url" name="edit_custom_url" placeholder="Enter custom url">
                            <span class="text-danger" id="edit_url_error"></span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" id="edit_url_btn" name="edit_url_btn">Edit URL</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    {{--Edit URL Modal--}}

    {{--Delete URL Modal--}}
    <div class="modal fade" id="deleteUrlModal" tabindex="-1" aria-labelledby="deleteUrlModalLabel" aria-hidden="true">
        <form action="" method="POST" id="delete_url_form">
            @csrf
            @method('DELETE')
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteUrlModalLabel">Delete A URL</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to delete this Url?</p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" id="delete_url_btn" name="delete_url_btn">Delete URL</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    {{--Delete URL Modal--}}
@endsection

@section('page-level-styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.css">
@endsection

@section('page-level-scripts')
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>
<script>
    /*
    * Fill the edit modal with url details
    * */
    function fillEditUrlDetails(element)
    {
        var id = $(element).data('id');

        var url = "{{route('user.urls.edit', ['@', '@'])}}".replace('@', "{{\Illuminate\Support\Facades\Auth::user()->id}}").replace('@', id);
        $.ajax({
            url: url,
            method: 'GET',
            success: function (data){
                data = JSON.parse(data);
                console.log(data);
                $('#edit_url_id').val(data.id);
                $('#edit_url_title').val(data.title);
                $('#edit_actual_url').val(data.actual_url);
                $('#edit_custom_url').val(data.slug);

                $('#editUrlModal').modal('show');

                var urlLink = "{{route('user.urls.update', ['@', '@'])}}".replace('@', "{{\Illuminate\Support\Facades\Auth::user()->id}}").replace('@', id);
                $("#edit_url_form").attr('action', urlLink);
            },
            error: function (data){
                alert(data);
            }
        });
    }

    /*
    * Fill the delete modal with url details
    * */
    function fillDeleteUrlDetails(element)
    {
        var id = $(element).data('id');
        var urlLink = "{{route('user.urls.destroy', ['@', '@'])}}".replace('@', "{{\Illuminate\Support\Facades\Auth::user()->id}}").replace('@', id);
        $("#delete_url_form").attr('action', urlLink);
        $('#deleteUrlModal').modal('show');
    }

    $(function (){

        /**
         * To initialize datatable
         * */
        $('#table_id').DataTable();

        /**
         * To validate the create Url form details
         * */
        $('#create_url_form').validate({
           rules: {
               "title": {
                   required: true,
               },
               "actual_url": {
                   required: true,
                   url: true,
               },
               "custom_url_input": {
                   required: true,
               },
           },
           submitHandler: function (form) {

               var slug = $('#custom-url-input').val();
               var url = "{{route('slugCheck')}}";
               console.log(url);
               $.ajax({
                   url: url,
                   method: "POST",
                   data: {
                       _token: "{{ csrf_token() }}",
                       slug: slug
                   },
                   success: function (data){
                       console.log(data);
                       // return;
                       if(data[0]){
                           form.submit();
                           // $('#create_url_form').submit();
                           {{--window.location.href = "{{route('user.index')}}";--}}
                       }else{
                           $('#create_url_error').text('Url is already been taken!')
                       }
                   },
                   error: function (data){
                       alert(data);
                   }
               });

           }
       });

        /**
         * To validate the edit Url form details
         * */
        $('#edit_url_form').validate({
            rules: {
                "edit_url_title": {
                    required: true,
                },
                "edit_custom_url": {
                    required: true,
                },
            },
            submitHandler: function (form) {

                var slug = $('#edit_custom_url').val();
                var id = $('#edit_url_id').val();
                console.log(id);
                var url = "{{route('slugCheck')}}";
                console.log(url);
                $.ajax({
                    url: url,
                    method: "POST",
                    data: {
                        _token: "{{ csrf_token() }}",
                        slug: slug,
                        id: id,
                    },
                    success: function (data){
                        // console.log(data[0]);
                        // return;
                        if(data[0]){
                            form.submit();
                            // $('#edit_url_form').submit();
                            {{--window.location.href = "{{route('user.index')}}";--}}
                        }else{
                            $('#edit_url_error').text('Url is already been taken!')
                        }
                    }
                });

                // form.submit();
            }
        });
    });

    /**
     * To remove the error of previously taken slug for next time
     * */
    $('#edit_custom_url').on('change', function (){
       $('#edit_url_error').text('');
    });

</script>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script src="{{asset('js/users/index.js')}}"></script>
<script>
    initMostViewedUrlGraph('{!! json_encode($mostViewedUrls->toArray()) !!}');
    initViewsByMonthGraph('{!! json_encode($viewsByMonth) !!}');
</script>
@endsection

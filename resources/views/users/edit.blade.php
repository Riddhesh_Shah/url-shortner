@extends('layouts.app')

@section('page-title')
Edit Profile
@endsection

@section('content')
<div class="registration-card mt-5">
    <div class="card">
        <form method="POST" action="{{ route('user.update', \Illuminate\Support\Facades\Auth::user()) }}" id="registration-form">
            @csrf
            @method('PUT')
            <div class="card-header">
                <div class="d-flex justify-content-between">
                    <span>Registration Form</span>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name" class="form-label mt-4">Name <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Enter name" value="{{$user->name}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="username" class="form-label mt-4">Username <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="username" name="username" placeholder="Enter username" value="{{$user->username}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="d-flex justify-content-between">
                                <label for="phone" class="form-label mt-4">
                                    Phone Number <span class="text-danger">*</span>
                                    <small class="text-muted">(We'll never share your phone number with anyone else.)</small>
                                </label>
                                <span class="align-self-end mb-2 text-success" id="verify-phone-btn" data-bs-toggle="modal" data-bs-target="#phoneVerificationModal" onclick="setPhoneInModal(this)">Edit</span>
                            </div>
                            <input type="text" class="form-control" id="phone" name="phone" aria-describedby="phone-notice" placeholder="Enter phone number" value="{{$user->phone}}" data-phone-verified="false" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="d-flex justify-content-between">
                                <label for="email" class="form-label mt-4">
                                    Email <span class="text-danger">*</span>
                                    <small class="text-muted">(We'll never share your email with anyone else.)</small>
                                </label>
                                <span class="align-self-end mb-2 text-success" id="verify-email-btn" data-bs-toggle="modal" data-bs-target="#emailVerificationModal" onclick="setEmailInModal(this)">Edit</span>
                            </div>
                            <input type="email" class="form-control" id="email" name="email" aria-describedby="phone-notice" placeholder="Enter email" value="{{$user->email}}" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="d-flex justify-content-end">
                    <button class="btn btn-primary" id="register-btn" type="submit">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>


{{--Phone Registration Modal--}}
<div class="modal fade" id="phoneVerificationModal" tabindex="-1" aria-labelledby="phoneVerificationModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="phoneVerificationModalLabel">Verify your Phone Number</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="phone-input">
                    <label for="unverified_phone_number" class="form-label mt-4">
                        Phone Number <span class="text-danger">*</span>
                        <small class="text-muted">(Please verify phone number is correct.)</small>
                    </label>
                    <input type="text" class="form-control" id="unverified_phone_number" name="unverified_phone_number" placeholder="9999999999">
                </div>
                <div class="otp-input-card">
                    <label for="otp-input" class="form-label mt-4">
                        OTP <span class="text-danger">*</span>
                        <small class="text-muted">(Received on your phone number.)</small>
                    </label>
                    <input type="text" class="form-control" id="otp-input" name="otp-input">
                    <span id="otp-error" class="text-danger"></span>
                </div>
                <div id="recaptcha-container" class="mt-4"></div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" id="get-otp-btn" onclick="sendOtp()">Get OTP</button>
                <button class="btn btn-success" id="verify-otp-btn" onclick="verifyOtp()" disabled="disabled">Verify OTP</button>
            </div>
        </div>
    </div>
</div>
{{--Phone Registration Modal--}}
@endsection

@section('page-level-scripts')
    <script>
        function setPhoneInModal(element) {
            console.log("ji");
            /* Updating the phone number in the modal */
            var phoneNumber = $("#phone").val();
            $("#unverified_phone_number").val(phoneNumber);
        }

        function sendOtp(){
            $.ajax({
                url: "{{route('send-otp')}}",
                method: "POST",
                data: {
                    "_token": "{{csrf_token()}}",
                    "phone": $("#unverified_phone_number").val(),
                },
                success: function (data){
                    if(data[0]){
                        $('#verify-otp-btn').removeAttr('disabled');
                        console.log("OTP Sent");
                    }else{
                        console.log('Failed');
                    }
                }
            });
        }

        function verifyOtp(){
            $.ajax({
                url: "{{route('verify-otp')}}",
                method: "POST",
                data: {
                    "_token": "{{csrf_token()}}",
                    "phone": $("#unverified_phone_number").val(),
                    "otp": $("#otp-input").val(),
                },
                success: function (data){
                    if(data[0]){
                        /**
                         * User Signed in successfully (Now steps to be performed)
                         * 1. Fill the phone number into the field
                         * 2. Disable the field from editing
                         * 3. Remove the verify Button
                         * 4. Change the UI of the Input Field
                         * 5. Enable the Register button
                         * 6. Add a data attribute 'data-validated' for further use
                         * 7. Close the modal
                         */
                        /*Step 1*/

                        var phoneNumber = $('#unverified_phone_number').val();
                        $('#phone').val(phoneNumber);

                        /*Step 2*/
                        $('#phone').attr('readonly', 'readonly');

                        /*Step 3*/
                        $('#verify-phone-btn').remove();

                        /*Step 4*/
                        $('#phone').addClass('is-valid');
                        $('#phone').attr('data-phone-verified', 'false');

                        /*Step 5*/
                        $('#register-btn').removeAttr('disabled');

                        /*Step 6*/
                        $('#phone').attr('data-validated', 'true');

                        /*Step 7*/
                        $(function () {
                            $('#phoneVerificationModal').modal('toggle');
                        });
                    }else{
                        console.log('Failed');
                    }
                }
            });
        }
    </script>
@endsection

@extends('layouts.app')

@section('page-title')
    Index
@endsection

@section('content')
    <div class="view-section mt-5">
        <div class="row">
            <div class="col-md-6">
                <div class="introduction-text">
                    <strong>Enterprise Class</strong>
                    <h1><strong>The trusted, unrivaled enterprise-grade solution</strong></h1>
                    <h4>One link management platform for all your needs.</h4>
                </div>
                <div class="get-quote-card mt-5">
                    <div class="btn btn-primary btn-lg">Get Quote</div>
                </div>
                <div class="watch-demo-card mt-2">
                    <a href="">Watch Enterprise Demo</a>
                </div>
                <div class="talk-to-sales-card mt-4">
                    <span><strong>Talk to Sales:</strong> +1 (718) 838-9412</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="overflow-hidden">
                    <img src="https://docrdsfx76ssb.cloudfront.net/static/1648662222/pages/wp-content/uploads/2019/03/enterprise01.jpg" alt="" width="100%">
                </div>
            </div>
        </div>
    </div>
    <div class="feature-section">
        <div class="row mt-5">
            <div class="col-md-4">
                <div class="card-image">
                    <img src="https://docrdsfx76ssb.cloudfront.net/static/1648662222/pages/wp-content/uploads/2019/12/integrations.svg" alt="">
                </div>
                <div class="card-content">
                    <h5 class="mt-3"><strong>Integrate with anything</strong></h5>
                    <p class="mt-3">Confidently build Bitly-powered links into even the most complex workflows and custom solutions. Join the over 300,000 active API users and two-thirds of the Fortune 500 that trust Bitly to support their service-critical programs.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card-image">
                    <img src="https://docrdsfx76ssb.cloudfront.net/static/1648662222/pages/wp-content/uploads/2019/03/icon-large-cloud-servers.min_.svg" alt="">
                </div>
                <div class="card-content">
                    <h5 class="mt-3"><strong>Over 120 billion clicks per year</strong></h5>
                    <p class="mt-3">Bitly knows what it takes to scale better than anyone. With an ultra-modern infrastructure capable of handling more than 10 billion clicks every month, Bitly is simply in a class by itself.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card-image">
                    <img src="https://docrdsfx76ssb.cloudfront.net/static/1648662222/pages/wp-content/uploads/2019/03/icon-large-chart-review.min_.svg" alt="">
                </div>
                <div class="card-content">
                    <h5 class="mt-3"><strong>Near-limitless speed</strong></h5>
                    <p class="mt-3">The Bitly API can generate and track thousands of unique links per minute—millions a day. Be it a massive SMS initiative, or other major program, Bitly has the proven capacity to handle it.</p>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-4">
                <div class="card-image">
                    <img src="https://docrdsfx76ssb.cloudfront.net/static/1648662222/pages/wp-content/uploads/2019/03/icon-large-database.min_.svg" alt="">
                </div>
                <div class="card-content">
                    <h5 class="mt-3"><strong>Unmatched record of reliability</strong></h5>
                    <p class="mt-3">While other link shorteners have come and gone, Bitly has amassed a 12-year, 99.9% uptime record. Deep experience matters when downtime is not an option, and Bitly provides peace of mind.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card-image">
                    <img src="https://docrdsfx76ssb.cloudfront.net/static/1648662222/pages/wp-content/uploads/2019/03/icon-large-clock-security.min_.svg" alt="">
                </div>
                <div class="card-content">
                    <h5 class="mt-3"><strong>Setting the standard for security</strong></h5>
                    <p class="mt-3">We set the bar more than a decade ago when it comes to link management security. Today, our 24 hour on-call teams continue to monitor the availability and performance of your links all day, every day. And of course our platform is fully GDPR compliant and CCPA ready.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card-image">
                    <img src="https://docrdsfx76ssb.cloudfront.net/static/1648662222/pages/wp-content/uploads/2019/03/icon-large-laptop-user.min_.svg" alt="">
                </div>
                <div class="card-content">
                    <h5 class="mt-3"><strong>Up, running, and under your control</strong></h5>
                    <p class="mt-3">Your personal success manager—included with Enterprise and Developer plans—ensures your team is ready from day one. Plus, Bitly’s enterprise-grade admin features let you centrally set up and manage access, as well as segment activity and analytics by department, location, channel, brand or user.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="client-section mt-5">
        <div class="card-heading text-center">
            <h2><strong>The most recognized brands in the world love Bitly</strong></h2>
        </div>
        <div class="card-section mt-4">
            <div class="row">
                <div class="col-md-2"><img src="https://docrdsfx76ssb.cloudfront.net/static/1648662222/pages/wp-content/uploads/2019/03/recognizable-brands-espn.svg" alt=""></div>
                <div class="col-md-2"><img src="https://docrdsfx76ssb.cloudfront.net/static/1648662222/pages/wp-content/uploads/2019/03/recognizable-brands-disney.svg" alt=""></div>
                <div class="col-md-2"><img src="https://docrdsfx76ssb.cloudfront.net/static/1648662222/pages/wp-content/uploads/2019/03/recognizable-brands-amazon.svg" alt=""></div>
                <div class="col-md-2"><img src="https://docrdsfx76ssb.cloudfront.net/static/1648662222/pages/wp-content/uploads/2019/03/recognizable-brands-buzzfeed.svg" alt=""></div>
                <div class="col-md-2"><img src="https://docrdsfx76ssb.cloudfront.net/static/1648662222/pages/wp-content/uploads/2019/03/recognizable-brands-nytimes.svg" alt=""></div>
                <div class="col-md-2"><img src="https://docrdsfx76ssb.cloudfront.net/static/1648662222/pages/wp-content/uploads/2019/03/recognizable-brands-bose.svg" alt=""></div>
            </div>
        </div>
    </div>
@endsection

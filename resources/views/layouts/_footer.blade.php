<footer id="sticky-footer" class="flex-shrink-0 py-4 bg-primary text-white mt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <ul class="list-unstyled mt-5">
                    <li class="mb-2"><strong>Why Bitly?</strong></li>
                    <li>Bitly 101</li>
                    <li>Integrations & API</li>
                    <li>Enterprise Class</li>
                    <li>Pricing</li>
                </ul>
                <ul class="list-unstyled mt-5">
                    <li class="mb-2"><strong>Company</strong></li>
                    <li>About Bitly</li>
                    <li>Careers</li>
                    <li>Partner</li>
                    <li>Press</li>
                    <li>Contact</li>
                    <li>Reviews</li>
                </ul>
            </div>
            <div class="col-md-3">
                <ul class="list-unstyled mt-5">
                    <li class="mb-2"><strong>Why Bitly?</strong></li>
                    <li>Bitly 101</li>
                    <li>Integrations & API</li>
                    <li>Enterprise Class</li>
                    <li>Pricing</li>
                </ul>
                <ul class="list-unstyled mt-5">
                    <li class="mb-2"><strong>Company</strong></li>
                    <li>About ShorturL</li>
                    <li>Career</li>
                    <li>Partner</li>
                    <li>Contact</li>
                    <li>Reviews</li>
                </ul>
            </div>
            <div class="col-md-3">
                <ul class="list-unstyled mt-5">
                    <li class="mb-2"><strong>Why Bitly?</strong></li>
                    <li>Bitly 101</li>
                    <li>Integrations & API</li>
                    <li>Enterprise Class</li>
                    <li>Pricing</li>
                </ul>
                <ul class="list-unstyled mt-5">
                    <li class="mb-2"><strong>Company</strong></li>
                    <li>About ShorturL</li>
                    <li>Career</li>
                    <li>Partner</li>
                    <li>Contact</li>
                    <li>Reviews</li>
                </ul>
            </div>
            <div class="col-md-3">
                <div class="mt-5">
                    <h3>ShorturL</h3>
                    <small>© 2022 ShorturL |
                        Handmade in San Francisco, Denver, New York City, Bielefeld, and all over the world.</small>
                </div>
                <div class="mt-3">
                    <a href="" class="mr-3"><span class="fab fa-2x fa-facebook"></span></a>
                    <a href="" class="mr-3"><span class="fab fa-2x fa-instagram"></span></a>
                    <a href="" class="mr-3"><span class="fab fa-2x fa-twitter"></span></a>
                    <a href="" class="mr-3"><span class="fab fa-2x fa-pinterest"></span></a>
                </div>
            </div>
        </div>
    </div>
</footer>

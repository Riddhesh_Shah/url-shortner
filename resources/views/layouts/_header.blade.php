{{--{{session()->flush()}}--}}
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{route('home')}}">ShorturL</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        @if(\Illuminate\Support\Facades\Auth::user() != null)
            <div class="collapse navbar-collapse">
            <ul class="navbar-nav me-auto">
                <li class="nav-item">
                    <a class="nav-link active" href="{{route('user.index', \Illuminate\Support\Facades\Auth::user())}}">Home
                        <span class="visually-hidden">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a type="button" class="nav-link" data-bs-toggle="modal" data-bs-target="#createUrlModal">Create a URL</a>
                </li>
            </ul>

            <div class="d-flex justify-content-end">
                <div class="mr-5">
                    <ul class="navbar-nav me-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">{{\Illuminate\Support\Facades\Auth::user()->name}}</a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="{{route('user.edit', \Illuminate\Support\Facades\Auth::user())}}">Edit Profile</a>
                                <div class="dropdown-divider"></div>
                                <form action="{{route('logout')}}" method="POST">
                                    @csrf
                                    <button class="dropdown-item" type="submit">Logout</button>
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        @else
            <div class="collapse navbar-collapse">
                <ul class="navbar-nav me-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Why ShorturL?</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item">ShorturL 101</a>
                            <a class="dropdown-item">Integrations & API</a>
                            <a class="dropdown-item">Enterprise Class</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Solutions</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item"><span class="fa fa-pen mr-1"></span>Social Media</a>
                            <a class="dropdown-item">Digital Marketing</a>
                            <a class="dropdown-item">Customer Service</a>
                            <a class="dropdown-item">For Developers</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a type="button" class="nav-link">Features</a>
                    </li>
                    <li class="nav-item">
                        <a type="button" class="nav-link">Pricing</a>
                    </li>
                    <li class="nav-item">
                        <a type="button" class="nav-link">Resources</a>
                    </li>
                </ul>

                <div class="d-flex justify-content-end">
                    <div class="mr-5">
                        <ul class="navbar-nav me-auto">
                            <li class="nav-item">
                                <a type="button" href="{{route('login')}}" class="nav-link">Log in</a>
                            </li>
                            <li class="nav-item">
                                <a type="button" href="{{route('register')}}" class="nav-link">Sign up</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        @endif
    </div>
</nav>

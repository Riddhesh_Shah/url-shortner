@extends('layouts.app')

@section('page-title')
    Register
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card mt-5">
                <div class="card-header">
                    <div class="d-flex justify-content-between">
                        <span>Registration Form</span>
                        <span><a href="{{route('login')}}">Login</a></span>
                    </div>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name" class="form-label mt-4">Name <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter name" value="Riddhesh Shah">
                                    @error('name')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="username" class="form-label mt-4">Username <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="username" name="username" placeholder="Enter username" value="riddhesh_shah">
                                    @error('username')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="d-flex justify-content-between">
                                        <label for="phone" class="form-label mt-4">
                                            Phone Number <span class="text-danger">*</span>
                                            <small class="text-muted">(We'll never share your phone number with anyone else.)</small>
                                        </label>
                                        <span class="align-self-end mb-2 text-success" id="verify-phone-btn" data-bs-toggle="modal" data-bs-target="#phoneVerificationModal" onclick="setPhoneInModal(this)">Verify</span>
                                    </div>
                                    <input type="text" class="form-control" id="phone" name="phone" aria-describedby="phone-notice" placeholder="Enter phone number" value="9769069674" data-phone-verified="false">
                                    @error('phone')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="d-flex justify-content-between">
                                        <label for="email" class="form-label mt-4">
                                            Email <span class="text-danger">*</span>
                                            <small class="text-muted">(We'll never share your email with anyone else.)</small>
                                        </label>
                                        <span class="align-self-end mb-2 text-success" id="verify-email-btn" data-bs-toggle="modal" data-bs-target="#emailVerificationModal" onclick="setEmailInModal(this)">Verify</span>
                                    </div>
                                    <input type="email" class="form-control" id="email" name="email" aria-describedby="phone-notice" placeholder="Enter email" value="riddhesh@gmail.com">
                                    @error('email')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="password" class="form-label mt-4">Password <span class="text-danger">*</span></label>
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Enter password" value="ridz1203">
                                    @error('password')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="password-confirm" class="form-label mt-4">Confirm Password <span class="text-danger">*</span></label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password" value="ridz1203">
                                    @error('password_confirmation')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6"></div>
                            <div class="col-md-6 mt-4">
                                <div class="d-flex justify-content-end">
                                    <button class="btn btn-dark" id="register-btn" type="submit" disabled="disabled">Register</button>
                                </div>
                            </div>
                        </div>
                        {{--<div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-end">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>--}}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

{{--Phone Registration Modal--}}
<div class="modal fade" id="phoneVerificationModal" tabindex="-1" aria-labelledby="phoneVerificationModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="phoneVerificationModalLabel">Verify your Phone Number</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="phone-input">
                    <label for="unverified_phone_number" class="form-label mt-4">
                        Phone Number <span class="text-danger">*</span>
                        <small class="text-muted">(Please verify phone number is correct.)</small>
                    </label>
                    <input type="text" class="form-control" id="unverified_phone_number" name="unverified_phone_number" placeholder="+919999999999" readonly>
                </div>
                <div class="otp-input-card">
                    <label for="otp-input" class="form-label mt-4">
                        OTP <span class="text-danger">*</span>
                        <small class="text-muted">(Received on your phone number.)</small>
                    </label>
                    <input type="text" class="form-control" id="otp-input" name="otp-input">
                    <span id="otp-error" class="text-danger"></span>
                </div>
                <div id="recaptcha-container" class="mt-4"></div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" id="get-otp-btn" onclick="sendOtp()">Get OTP</button>
                <button class="btn btn-success" id="verify-otp-btn" onclick="verifyOtp()" disabled="disabled">Verify OTP</button>
            </div>
        </div>
    </div>
</div>
{{--Phone Registration Modal--}}
@endsection

@section('page-level-scripts')
    <script>
        function setPhoneInModal(element) {
            console.log("ji");
            /* Updating the phone number in the modal */
            var phoneNumber = $("#phone").val();
            $("#unverified_phone_number").val(phoneNumber);
        }

        function sendOtp(){
            $.ajax({
                url: "{{route('send-otp')}}",
                method: "POST",
                data: {
                    "_token": "{{csrf_token()}}",
                    "phone": $("#unverified_phone_number").val(),
                },
                success: function (data){
                   if(data[0]){
                       $('#verify-otp-btn').removeAttr('disabled');
                       console.log("OTP Sent");
                   }else{
                       console.log('Failed');
                   }
                }
            });
        }

        function verifyOtp(){
            $.ajax({
                url: "{{route('verify-otp')}}",
                method: "POST",
                data: {
                    "_token": "{{csrf_token()}}",
                    "phone": $("#unverified_phone_number").val(),
                    "otp": $("#otp-input").val(),
                },
                success: function (data){
                    if(data[0]){
                        /**
                          * User Signed in successfully (Now steps to be performed)
                          * 1. Fill the phone number into the field
                          * 2. Disable the field from editing
                          * 3. Remove the verify Button
                          * 4. Change the UI of the Input Field
                          * 5. Enable the Register button
                          * 6. Add a data attribute 'data-validated' for further use
                          * 7. Close the modal
                        */
                        /*Step 1*/

                        var phoneNumber = $('#unverified_phone_number').val();
                        $('#phone').val(phoneNumber);

                        /*Step 2*/
                        $('#phone').attr('readonly', 'readonly');

                        /*Step 3*/
                        $('#verify-phone-btn').remove();

                        /*Step 4*/
                        $('#phone').addClass('is-valid');
                        $('#phone').attr('data-phone-verified', 'false');

                        /*Step 5*/
                        $('#register-btn').removeAttr('disabled');

                        /*Step 6*/
                        $('#phone').attr('data-validated', 'true');

                        /*Step 7*/
                        $(function () {
                            $('#phoneVerificationModal').modal('toggle');
                        });
                    }else{
                        console.log('Failed');
                    }
                }
            });
        }
    </script>
@endsection

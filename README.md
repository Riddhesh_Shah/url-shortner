# ShotrUrl - Url Shortner

## Description
<p>
It is a URL shortner which accepts Url and returns a ShortUrl this project has complete manual authentication using OTP and messages to avoid fake mobile numbers to be inserted while login
</p>

## Output Screens

### Landing Page
![img.png](img.png)

### Index Page
![img_1.png](img_1.png)
![img_2.png](img_2.png)

### Create Page
![img_3.png](img_3.png)

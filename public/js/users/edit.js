/*import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.6.9/firebase-app.js'

const firebaseConfig = {
    apiKey: "AIzaSyCYyZekFhhHP6RGJjdH15GCF2xj-EYD_AE",
    authDomain: "shorturl-84f2e.firebaseapp.com",
    projectId: "shorturl-84f2e",
    storageBucket: "shorturl-84f2e.appspot.com",
    messagingSenderId: "1009462120571",
    appId: "1:1009462120571:web:8efb8a9808d292fba1d2de",
    measurementId: "G-1XBRP8E4BW"
};

// Initialize Firebase
const firebase = initializeApp(firebaseConfig);
console.log(firebase);*/

import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.0.0/firebase-app.js';
import { getAuth, RecaptchaVerifier, signInWithPhoneNumber } from "https://www.gstatic.com/firebasejs/9.0.0/firebase-auth.js";

const firebaseConfig = {
    apiKey: "AIzaSyCYyZekFhhHP6RGJjdH15GCF2xj-EYD_AE",
    authDomain: "shorturl-84f2e.firebaseapp.com",
    projectId: "shorturl-84f2e",
    storageBucket: "shorturl-84f2e.appspot.com",
    messagingSenderId: "1009462120571",
    appId: "1:1009462120571:web:8efb8a9808d292fba1d2de",
    measurementId: "G-1XBRP8E4BW"
};
const firebase = initializeApp(firebaseConfig);

const auth = getAuth(firebase);
window.recaptchaVerifier = new RecaptchaVerifier('recaptcha-container', {}, auth);

recaptchaVerifier.render().then((widgetId) => {
    window.recaptchaWidgetId = widgetId;
    console.log(widgetId);
});

/*const recaptchaResponse = grecaptcha.getResponse(recaptchaWidgetId);
console.log(grecaptcha.getResponse(recaptchaWidgetId));*/

export function getOtp()
{
    const phoneNumber = $("#unverified_phone_number").val();
    const appVerifier = window.recaptchaVerifier;

    const auth = getAuth(firebase);
    signInWithPhoneNumber(auth, phoneNumber, appVerifier)
        .then((confirmationResult) => {
            // SMS sent. Prompt user to type the code from the message, then sign the
            // user in with confirmationResult.confirm(code).
            window.confirmationResult = confirmationResult;
            // ...
            console.log(confirmationResult);
        }).catch((error) => {
        // Error; SMS not sent
        // ...
    });
}

$('#get-otp-btn').on('click', function (){
    const phoneNumber = $("#unverified_phone_number").val();
    const appVerifier = window.recaptchaVerifier;

    const auth = getAuth(firebase);
    signInWithPhoneNumber(auth, phoneNumber, appVerifier)
        .then((confirmationResult) => {
            /**
             * OTP SMS sent to user successfully (Now steps to be performed)
             * 1. Remove the Captcha
             * 2. Enable Verify OTP Button
             */
            /*Step to store the result came from Firebase*/
            window.confirmationResult = confirmationResult;

            /*Step 1*/
            $('#recaptcha-container').remove();

            /*Step 2*/
            $('#verify-otp-btn').removeAttr('disabled');
        }).catch((error) => {
        // Error; SMS not sent
        // ...
        console.log(error);
    });
});

$('#verify-otp-btn').on('click', function (){
    const code = $('#otp-input').val();
    confirmationResult.confirm(code).then((result) => {
        /**
         * User Signed in successfully (Now steps to be performed)
         * 1. Fill the phone number into the field
         * 2. Disable the field from editing
         * 3. Remove the verify Button
         * 4. Change the UI of the Input Field
         * 5. Enable the Register button
         * 6. Add a data attribute 'data-validated' for further use
         * 7. Close the modal
         */
        const user = result.user;
        console.log(user);

        /*Step 1*/
        console.log(user['phoneNumber']);
        var phoneNumber = user['phoneNumber'];
        $('#phone').val(phoneNumber);

        /*Step 2*/
        $('#phone').attr('readonly', 'readonly');

        /*Step 3*/
        $('#verify-phone-btn').remove();

        /*Step 4*/
        $('#phone').addClass('is-valid');
        $('#phone').attr('data-phone-verified', 'false');

        /*Step 5*/
        var phoneValue = $('#unverified_phone_number').val();
        $('#phone').val(phoneValue);

        /*Step 6*/
        $('#phone').attr('data-validated', 'true');

        /*Step 7*/
        $(function () {
            $('#phoneVerificationModal').modal('toggle');
        });
    }).catch((error) => {
        // User couldn't sign in (bad verification code?)
        // ...
        $('#otp-error').val("Invalid OTP");
        console.log(error);
    });
});

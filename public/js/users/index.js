function initMostViewedUrlGraph(data)
{
    data = JSON.parse(data); //Converting to JS
    data = Object.values(data) //Converting to Array
    actual_urls = [];
    hits = [];
    titles = [];

    data.forEach(function (url){
        actual_urls.push(url.actual_url);
        hits.push(url.hits)
        titles.push(url.title)
    })

    data = {
        labels: titles,
        datasets: [{
            label: 'Most Viewed URLs',
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(255, 99, 132)',
            data: hits,
        }]
    };

    const config = {
        type: 'line',
        data: data,
        options: {}
    };

    const myChart = new Chart(
        document.getElementById('most_visited_urls_chart'),
        config
    );

}

function initViewsByMonthGraph(data)
{
    urls = JSON.parse(data); //Converting to JS
    urls = Object.values(urls);

    data = {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        datasets: [{
            label: 'Views on URLs by Month',
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(255, 99, 132)',
            data: urls,
        }]
    };

    const config = {
        type: 'line',
        data: data,
        options: {}
    };

    const myChart = new Chart(
        document.getElementById('views_by_month_chart'),
        config
    );
}

function customUrlField(element)
{
    if($(element).prop('checked')){
        $('#custom-url-card').addClass('mt-3');
        $('#custom-url-card').html(`
            <label for="name" class="form-label">Custom URL <span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="custom-url-input" name="custom_url_input" placeholder="Enter custom url">
            <span class="text-danger" id="create_url_error"></span>
        `);
    }else{
        $('#custom-url-card').removeClass('mt-3');
        $('#custom-url-card').html(``);
    }
}

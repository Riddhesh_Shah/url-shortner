<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Auth::routes();

//Route::get("/user/register", [\App\Http\Controllers\UsersController::class, 'register'])->name('user.register');
//Route::get("/user/login", [\App\Http\Controllers\UsersController::class, 'login']);

Route::resource('user', \App\Http\Controllers\UsersController::class);
Route::resource('user.urls', \App\Http\Controllers\UrlsController::class);
Route::post('/unique', [\App\Http\Controllers\UrlsController::class, 'uniqueSlugCheck'])->name('slugCheck');
Route::post("/send_otp", [\App\Http\Controllers\UsersController::class, 'sendOtp'])->name('send-otp');
Route::post("/verify_otp", [\App\Http\Controllers\UsersController::class, 'verifyOtp'])->name('verify-otp');

//Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');






Route::get("/{slug}",[\App\Http\Controllers\UrlsController::class, 'redirectToUrl']);

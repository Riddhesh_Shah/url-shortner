<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class Url extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $guarded = [];

    private static array $allowedCharacters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "_"];

    /*
     * Relationship Methods
     * */
    public function urlDetails(): HasMany
    {
        return $this->hasMany(UrlDetail::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /*
     * Helper Functions
     * */
    public function clickCount(): int
    {
        return count($this->urlDetails);
    }

    public static function slugToUrl($slug)
    {
        return Url::where('slug', $slug)->first();
    }

    public static function generateSlug()
    {
        $urls = Url::all();
        while (true) {
            $slug = strtolower(implode("", Arr::random(self::$allowedCharacters, random_int(1, 10))));
            if ($urls->where('slug', $slug)->first() == null) {
                return $slug;
            }
        }
    }

    /*
     * This function generates random slugs
     * It generates slugs on the basis of lengths of slugs
     * It keep tracks on previous lengths of slugs
     * Increases the number of characters if all the combinations are over
     *
     public static function generateSlug()
    {
        $slugCharacterCount = Url::latest('id')->first() == null ? 1 : strlen(Url::latest()->first()->slug);
        $totalCharactersAllowed = count(self::$allowedCharacters);
        $numberOfUrlsPossible = $totalCharactersAllowed ** $slugCharacterCount;
        $urls = Url::all();
        $numberOfUrlsCreated = count($urls);

        if($numberOfUrlsPossible < $numberOfUrlsCreated + 1){
            $slugCharacterCount = $slugCharacterCount + 1;
        }

        while (true) {

            $slug = "";
            $count = $slugCharacterCount;

            while ($count--){
                $slug .= Arr::random(self::$allowedCharacters);
            }

            if ($urls->where('slug', $slug)->first() == null) {
                return $slug;
            }
        }

    }
    */

    public function hitIncrement(int $amount=1)
    {
        $hits = $this->hits;
        $this->update([
           "hits" =>  $hits + $amount
        ]);
    }

}

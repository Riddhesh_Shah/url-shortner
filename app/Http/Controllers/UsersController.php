<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use Carbon\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;
use Twilio\Rest\Client;

class UsersController extends Controller
{

    public function index(): View
    {
//        Auth::logout();
//        $user = User::where("id",1)->first();
        $user = Auth::user();
        $urls = $user->urls;

        /*Most View URLs*/
        $mostViewedUrls = $urls->sortByDesc('hits')->take(10);

        /*Views By Month*/
        $viewsByMonth = [];
        $year = Carbon::now()->year;

        for ($i=0;$i<12;$i++){
            $num = 0;
            $monthlyUrls = $urls->whereBetween("created_at", [date($year.'-'.($i+1)), date($year.'-'.($i+2))])->all();
            foreach ($monthlyUrls as $url){
                $num += $url->hits;
            }
            $viewsByMonth[$i+1] = $num;
        }
//        dd($viewsByMonth);

        return view('users.index', compact([
            'urls',
            'mostViewedUrls',
            'viewsByMonth',
        ]));
    }

    public function store(Request $request)
    {
//        dd($request);
        /**
         * Validation
         */
        User::create([
            "name" => $request->name,
            "email" => $request->email,
            "number" => $request->phone,
            "username" => $request->username,
            "password" => Hash::make($request->password),
        ]);

        return redirect(route('user.index'));
    }

    public function edit(): View
    {
//        dd(auth()->user());
        $user = Auth::user();
        return view('users.edit', compact(
            'user',
        ));
    }

    public function update(Request $request, User $user)
    {
//        dd($request->yo);
        $data = [];
        if($request->name){
            $data['name'] = $request->name;
        }
        if($request->username){
            $data['username'] = $request->username;
        }
        if($request->phone){
            $data['phone'] = $request->phone;
        }
        if($request->email){
            $data['email'] = $request->email;
        }

        if($data != null){
            $user->update($data);
        }

        return redirect(route('user.index'));
    }

    public function register(): View
    {
        return view('users.register');
    }

    public function login(): View
    {
        return view('users.login');
    }

    public function sendOtp(Request $request)
    {
//        dd("hello");
        $number = $request->phone;
        $timeInSeconds = 10*60;
        $api = "m0O7VUCQv1DpBGKSMJdjyXx28P369slerEzAHZFbkNthf4gwiofEU7MuQoigTF38aRWXp90dbKe5krvS";

        if(session()->exists("user")){
            if(time() - session()->get("user")['start'] > $timeInSeconds){
                session()->forget("user");
                $otp = substr(str_shuffle("0123456789"), 0, 6);
            }else{
                //$otp = session()->get("user")['otp'];
                $otp = substr(str_shuffle("0123456789"), 0, 6);
            }
        }else{
            $otp = substr(str_shuffle("0123456789"), 0, 6);
        }
//        dd(session()->get($number));

        $sender_id = "TXTIND";
        $message = "Hello User\n Your OTP: $otp";

        $fields = array(
            "sender_id" => $sender_id,
            "message" => $message,
            "route" => "v3",
            "numbers" => $number,
        );

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://www.fast2sms.com/dev/bulkV2",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($fields),
            CURLOPT_HTTPHEADER => array(
                "authorization: $api",
                "accept: */*",
                "cache-control: no-cache",
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

//        $err = false;
        if ($err) {
            dd($err);
            return [false];
        } else {
            session()->put("00000009769069674", [
                "hello" => "hi",
                "yo" => "hi",
            ]);
            session()->put("user", [
                'start' => time(),
                'otp' => Hash::make($otp)
            ]);
//            dd(session()->get($number));
            return [true];
        }
    }

    public function verifyOtp(Request $request)
    {
        $number = $request->phone;
//        echo $number;
//        dd($number, session()->get($number), session()->get('test'), session()->all());
        $otp = $request->otp;
        $timeInSeconds = 10 * 60;
//        dd(session()->all());

        if(session()->exists("user")) {
            if (time() - session()->get('user')['start'] > $timeInSeconds) {
                session()->forget('user');
                return [false];
            }
        }

        return [session()->exists('user') && Hash::check($otp, session()->get('user')['otp'])];
    }
}

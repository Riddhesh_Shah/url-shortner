<?php

namespace App\Http\Controllers;

use App\Models\Url;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

class UrlsController extends Controller
{
    public function redirectToUrl($slug)
    {
        $url = Url::slugToUrl($slug);
        //Return if URL not found
//        dd($url == null);
        if($url == null){
//            dd($slug);
            return redirect(route('user.index'));
        }
        $url->hitIncrement();
        $ip = $_SERVER['REMOTE_ADDR'];
        $url->urlDetails()->create([
            'ip' => $ip,
            'location' => "India" //Need public IP to get location - ipinfodb.com
        ]);
        return Redirect::to("$url->actual_url");
    }

    public function uniqueSlugCheck(Request $request)
    {
        $url = Url::slugToUrl($request->slug);
        return [($url == null || $url->id == $request->id)];
    }

    public function store(Request $request, User $user){
        $slug = "";
        if($request->has('custom_url_check') && $request->custom_url_input != null){
            $slug = $request->custom_url_input;
        }else{
            $slug = Url::generateSlug();
        }
//        dd($request);

        $user->urls()->create([
            'title' => $request->title,
            'actual_url' => $request->actual_url,
            'slug' => $slug,
            'hits' => 0
        ]);
        return redirect(route('user.index'));
    }

    public function edit(Request $request, User $user, Url $url)
    {
        if ($user->id == $url->user_id) {
            return json_encode($url);
        }
        return redirect()->back();
    }

    public function update(Request $request, User $user, Url $url)
    {
        if ($user->id == $url->user_id) {
            $url->update([
                'title' => $request->edit_url_title,
                'slug' => $request->edit_custom_url,
            ]);
            return redirect(route('user.index'));
        }
        return redirect()->back();
    }

    public function destroy(Request $request, User $user, Url $url)
    {
        if($user->id == $url->user_id)
        {
            $url->delete();
            return redirect(route('user.index'));
        }
        return redirect()->back();
    }

}
